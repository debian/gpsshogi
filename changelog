gpsshogi (0.7.0-3.2) unstable; urgency=medium

  * Non-maintainer upload.
  * d/copyright: Convert to standardized format
  * d/copyright: Include EPL text (Closes: #1069119)

 -- Bastian Germann <bage@debian.org>  Tue, 16 Apr 2024 18:56:27 +0000

gpsshogi (0.7.0-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * No change source-only upload for testing migration.

 -- Boyuan Yang <byang@debian.org>  Tue, 27 Sep 2022 15:03:18 -0400

gpsshogi (0.7.0-3) unstable; urgency=medium

  * New maintainer (Closes: #964730).
  * Include a patch for latest svn changes, only impacting the "upstream"
    branch in salsa.
  * Switch away from CDBS, to full dh 12.
  * Remove extra -l flags for libs not directly used.
  * Fix build with current Qt5 versions.
  * Split gpsshogi-viewer into its own package (Closes: #874108).
  * Fix Vcs-* control fields for salsa.
  * Get rid or manual debian/control parsing (lintian).

 -- Yann Dirson <dirson@debian.org>  Sat, 19 Dec 2020 16:37:44 +0100

gpsshogi (0.7.0-2) unstable; urgency=medium

  * Fix compile errors on i386:
    - debian/patches/0005-Fix-compile-errors-on-i386.patch
    (Closes: #861644)

 -- Daigo Moriwaki <daigo@debian.org>  Mon, 17 Jul 2017 07:27:47 +0000

gpsshogi (0.7.0-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Import back again un-incorporated NMU  0.6.0-3+nmu2.  Closes: 848345
  * Use HTTPS in Vcs-* headers.
  * Apply change in git reposiotry from Daigo Moriwaki: specify QT_SELECT=qt5
    and add a patch to build with qt5.

 -- Mattia Rizzolo <mattia@debian.org>  Fri, 31 Mar 2017 00:57:08 +0200

gpsshogi (0.7.0-1) unstable; urgency=medium

  * New upstream release (r3043).
  * Remove patches that are no longer applicable:
    - debian/patches/20100206-qt-common.pro.patch
    - debian/patches/20100528-makefile_conf_remove_malloc.patch
  * Add new patches:
    - debian/patches/0001-Customize-qmake-files.patch
    - debian/patches/0002-Customize-make-files.patch
    - debian/patches/0003-Fix-compiler-warnings-with-GCC-6.patch
    (Closes: #811689)

 -- Daigo Moriwaki <daigo@debian.org>  Sun, 04 Dec 2016 10:15:52 +0900

gpsshogi (0.6.0-3+nmu2) unstable; urgency=medium

  * Non-maintainer upload.

  [ Steve Langasek ]
  * Remove hard-coded dependency on libosl1. (Closes: #795237)

  [ Bas Couwenberg ]
  * Update build dependencies for GSL 2, change libgsl0-dev to libgsl-dev.
    (Closes: #807204)

 -- Sebastian Ramacher <sramacher@debian.org>  Sat, 02 Jan 2016 17:16:11 +0100

gpsshogi (0.6.0-3+nmu1) unstable; urgency=medium

  * Non-maintainer upload.
  * Change boost build-dep to be version-less (Closes: #759390).
  * Add hint "Shogi" to menu file.

 -- Yann Dirson <dirson@debian.org>  Thu, 09 Oct 2014 22:23:13 +0200

gpsshogi (0.6.0-3) unstable; urgency=medium

  * debian/qt-common-local.pro.i386:
    - Stop using march to build on the i386 architecture.
    - Build without SSE.
  * Use ESP license in a text format since the previous html format had
    privacy-breach-google-adsense.

 -- Daigo Moriwaki <daigo@debian.org>  Sat, 08 Feb 2014 23:41:31 +0900

gpsshogi (0.6.0-2) unstable; urgency=low

  * Now build-depends on libboost1.54-all-dev.

 -- Daigo Moriwaki <daigo@debian.org>  Sun, 15 Sep 2013 21:18:30 +0900

gpsshogi (0.6.0-1) unstable; urgency=low

  * New upstream release (r2844).
  * Removed debian/patches/20120608-Makefile.patch, which has been applied by
    the upstream.
  * debian/control:
    -  Bumped up Standards-Version to 3.9.4.
    - Updated the corresponding libosl version.

 -- Daigo Moriwaki <daigo@debian.org>  Sun, 21 Jul 2013 21:58:29 +0900

gpsshogi (0.5.0-1) unstable; urgency=low

  * New upstream release (r2758).
  * Build-depends on the latest boost libraries (1.49.0).  (Closes: #672748)
  * debian/patches/20120121-r2689-gpsshell.patch: removed. It has been applied
    by the upstream.
  * debian/control:
    - Updated the corresponding libosl version.
    - Bumped up Standards-Version to 3.9.2.
  * Added a new patch: debian/patches/20120608-Makefile.patch
    - Build in a hardening-friendly way.

 -- Daigo Moriwaki <daigo@debian.org>  Fri, 25 May 2012 20:16:34 +0900

gpsshogi (0.4.3-1) unstable; urgency=low

  * New upstream release (r2688).
  * removed patches that the upstream adapted:
    - 20111010-r2604_gpsshell_makefile.patch
    - 20111010-r2603_viewer_pvModel.cc.patch
  * debian/control: Upgraded corresponding libosl version.
  * Added a patch: debian/patches/20120121-r2689-gpsshell.patch.

 -- Daigo Moriwaki <daigo@debian.org>  Sat, 21 Jan 2012 17:26:29 +0900

gpsshogi (0.4.2-1) unstable; urgency=low

  * New upstream release (r2602).
  * Added patches:
    - debian/patches/20111010-r2603_viewer_pvModel.cc.patch
    - debian/patches/20111010-r2604_gpsshell_makefile.patch

 -- Daigo Moriwaki <daigo@debian.org>  Mon, 10 Oct 2011 16:06:43 +0900

gpsshogi (0.4.0-1) unstable; urgency=low

  * New upstream release (r2566).
  * debian/control
    - Bumped up Standards-Version to 3.9.2.
    - Build with Boost 1.46.
    - Build with libreadline6-dev
  * updated debian/patches/20100206-qt-common.pro.patch.
  * removed debian/patches/20100525-viewer-version_r2364-2365.patch, which was
    applied by the stream.

 -- Daigo Moriwaki <daigo@debian.org>  Sat, 11 Jun 2011 18:29:02 +0900

gpsshogi (0.3.0-1) unstable; urgency=low

  * New upstream release (r2361).
  * Added debian/source/format file.
  * Added debian/README.source file.
  * Added debian/gpsshogi.menu to add gpsshogi-view into Menu.
  * Now man pages go to section 6 (for games).
  * Fixed a FTBFS error. (Closes: #582984)
  * Converted the patch system to quilt.
  * Added debian/patches
    - 20100525-viewer-version_r2364-2365.patch.
    - 20100528-makefile_conf_remove_malloc.patch.
  * Added a man page gpsshogi-viewer.1 for gpsshogi-viewer
    since running the command on the fly is hard due to an X11
    configuration.

 -- Daigo Moriwaki <daigo@debian.org>  Tue, 25 May 2010 21:49:28 +0900

gpsshogi (0.2.0-3) unstable; urgency=low

  * debian/control, debian:rules: Added sample/gpsshell.
  * debian/control: Bumped up Standards-Version to 3.8.4.

 -- Daigo Moriwaki <daigo@debian.org>  Sun, 07 Feb 2010 19:32:41 +0900

gpsshogi (0.2.0-2) unstable; urgency=low

  * debian/rules: Enable to use CDBS's simple patch system.
  * debian/patches/20100206-qt-common.pro.patch: Added a patch to
    ./qt-common.pro
  * Added gpsshogi-viewer

 -- Daigo Moriwaki <daigo@debian.org>  Sat, 06 Feb 2010 16:05:16 +0900

gpsshogi (0.2.0-1) unstable; urgency=low

  * Initial release. (Closes: #567621)

 -- Daigo Moriwaki <daigo@debian.org>  Sat, 30 Jan 2010 18:48:02 +0900
