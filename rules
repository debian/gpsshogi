#!/usr/bin/make -f

export DH_VERBOSE=1

DPKG_EXPORT_BUILDFLAGS = 1

include /usr/share/dpkg/buildflags.mk
CFLAGS += $(CPPFLAGS)
CXXFLAGS += $(CPPFLAGS)
LDFLAGS += -Wl,--as-needed

DEB_DBG_PACKAGE_gpsshogi = gpsshogi-dbg
DEB_INSTALL_DIRS_gpsshogi += usr/games
DEB_INSTALL_MANPAGES_gpsshogi += debian/gpsusi.1
DEB_INSTALL_MANPAGES_gpsshogi += debian/gpsshogi.1
DEB_INSTALL_MANPAGES_gpsshogi += debian/gpsshell.1
DEB_INSTALL_MANPAGES_gpsshogi += debian/gpsshogi-viewer.1
DEB_INSTALL_DOCS_gpsshogi += debian/epl-v10.txt
DEB_INSTALL_DIRS_gpsshogi_data += usr/share/gpsshogi-data/data

export OSL_HOME = /usr/share/libosl-dev
#OSL_HOME_FLAGS = -DOSL_HOME=\\\"/usr/share/gpsshogi-data\\\"
export FILE_TCMALLOC = ''

export QT_SELECT = qt5
export OSL_HOME_QT = ''
export TCMALLOC = ''

%:
	dh $@

override_dh_auto_configure:
	true # nothing for all those Makefiles
	# viewer
	dh_auto_configure --buildsystem=qmake --sourcedirectory=lib/gui
	dh_auto_configure --buildsystem=qmake --sourcedirectory=sample/viewer

override_dh_auto_build:
	# gpsusi, gpsshogi
	dh_auto_build --buildsystem=makefile --sourcedirectory=bin -- gpsshogi gpsusi \
		FILE_OSL_ALL='' LDLIBS='-losl -lboost_filesystem -lboost_system -ltcmalloc_minimal' RELEASE=t
	help2man --section 6 -n "GPSShogi for XShogi and the CSA protocol" --no-info \
	  $(CURDIR)/bin/gpsshogi > $(CURDIR)/debian/gpsshogi.1
	help2man --section 6 -n "GPSShogi for the USI protocol" --no-info \
	  $(CURDIR)/bin/gpsusi > $(CURDIR)/debian/gpsusi.1
	# gpsshell
	dh_auto_build --buildsystem=makefile --sourcedirectory=sample/gpsshell -- gpsshell \
		FILE_OSL_ALL='' LDLIBS='-losl -lboost_filesystem -lboost_system -ltcmalloc_minimal' RELEASE=t
	help2man --section 6 -n "GPSShogi shell like client" --version-string "gpsshell" --no-info \
	  $(CURDIR)/sample/gpsshell/gpsshell > $(CURDIR)/debian/gpsshell.1
	# viewer
	dh_auto_build --buildsystem=qmake --sourcedirectory=lib/gui
	dh_auto_build --buildsystem=qmake --sourcedirectory=sample/viewer
	$(MAKE) viewer-release
	mv sample/viewer/viewer sample/viewer/gpsshogi-viewer

	chrpath --delete bin/gpsusi bin/gpsshogi sample/gpsshell/gpsshell sample/viewer/gpsshogi-viewer

clean::
	for dir in lib/gui sample/viewer ; do \
	  [ ! -f $$dir/Makefile ] || $(MAKE) -C $$dir OSL_HOME=/usr/share/libosl-dev distclean ; \
	done
	for dir in lib bin sample/gpsshell ; do \
	  $(MAKE) -C $$dir OSL_HOME=/usr/share/libosl-dev clean ; \
	done
	-rm -f $(CURDIR)/sample/viewer/gpsshogi-viewer
	for file in gpsusi gpsshogi gpsshell ; do \
	  rm -rf $(CURDIR)/debian/$$file.1 ; \
	done

include /usr/share/dpkg/pkg-info.mk
SRCDIR_NAME = $(DEB_SOURCE)-$(DEB_VERSION_UPSTREAM).orig

get-orig-source:
	if [ ! -f gpsshogi.tar.gz ] ; then \
	  wget -O gpsshogi.tar.gz "http://gps.tanaka.ecc.u-tokyo.ac.jp/cgi-bin/viewvc.cgi/trunk/gpsshogi.tar.gz?root=gpsshogi&view=tar" ; \
        fi
	mkdir debian/$(SRCDIR_NAME)
	cd debian/$(SRCDIR_NAME) && tar xzvf ../../gpsshogi.tar.gz
	if test "`ls debian/$(SRCDIR_NAME) | wc -l`" -eq "1" ; then \
	        subdir=`echo debian/$(SRCDIR_NAME)/*` && \
		find $$subdir -mindepth 1 -maxdepth 1 -exec mv -t debian/$(SRCDIR_NAME) -- {} + && \
	        rmdir $$subdir ; \
	fi
	tar czf ../$(DEB_SOURCE)_$(DEB_VERSION_UPSTREAM).orig.tar.gz -C debian $(SRCDIR_NAME)
	rm -rf debian/$(SRCDIR_NAME)

